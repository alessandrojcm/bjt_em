//
// Created by aless on 07/04/2018.
//

#pragma once
#include <stdexcept>

class resistencia {
private:
  float _resistividad;

public:
  explicit resistencia(float _resistividad = 0);

  resistencia(const resistencia &resistencia);

  float resistividad() const {
    return _resistividad;
  }
};



