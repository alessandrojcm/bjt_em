//
// Created by aless on 09/04/2018.
//

#pragma once
#include <iomanip>
#include <sstream>

#include <nana/gui/widgets/form.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/msgbox.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/place.hpp>

#include "Input_box.h"
#include "Material_Selection.h"
#include "BJT_PNP_EC.h"
#include "resistencia.h"
#include "Realimentacion_de_voltaje.h"

class Main_Window : public nana::form {
private:
  // Componentes para el cálculo.
  std::shared_ptr<resistencia> rb{new resistencia(1)};
  std::shared_ptr<resistencia> rc{new resistencia(1)};
  std::shared_ptr<resistencia> re{new resistencia(1)};
  std::shared_ptr<BJT_PNP_EC> transistor{new BJT_PNP_EC(1, SILICIO)};
  Realimentacion_de_voltaje circuito;

  //Elementos de la interfaz de ususario.
  Input_box<resistencia> rb_input;
  Input_box<resistencia> rc_input;
  Input_box<resistencia> re_input;
  Input_box<int> vcc_input;

  Input_box<BJT_PNP_EC> transistor_input;
  Material_Selection material_selection;

  nana::label transistor_label;
  nana::button calculate;

  void calculate_circuit();

public:
  Main_Window(const nana::rectangle &fm, const nana::appearance &appear);
  ~Main_Window() override = default;
};



