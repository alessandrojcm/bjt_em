//
// Created by aless on 09/04/2018.
//

#pragma once
#include "BJT_PNP_EC.h"
#include <nana/gui/widgets/combox.hpp>
#include <nana/gui/wvl.hpp>
#include <nana/gui/widgets/form.hpp>

class Material_Selection: public nana::combox {
public:
  explicit Material_Selection(const nana::form& fm);

  MATERIAL get_material();
};