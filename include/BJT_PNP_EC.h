//
// Created by aless on 08/04/2018.
//

#pragma once
#include <stdexcept>

enum MATERIAL {
    SILICIO,
    GERMANIO
};

class BJT_PNP_EC {
private:
  float _vbe;
  int _beta;

  void set_material(const MATERIAL &mat);
public:
  explicit BJT_PNP_EC(const int &_beta = 1, const MATERIAL &mat = SILICIO);

  float vbe() const;
  int beta() const;

  void set_vbe(const MATERIAL &mat);
};



