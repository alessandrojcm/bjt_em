//
// Created by aless on 08/04/2018.
//

#pragma once
#include <memory>

#include "resistencia.h"
#include "BJT_PNP_EC.h"
#include "stdexcept"

class Realimentacion_de_voltaje {
private:
  resistencia _RC;
  resistencia _RB;
  resistencia _RE;
  BJT_PNP_EC _transistor;
  int _VCC;

public:
  Realimentacion_de_voltaje(const resistencia &RC,
                            const resistencia &RB,
                            const resistencia &RE,
                            const BJT_PNP_EC &transistor);
  ~Realimentacion_de_voltaje() = default;

  float ib() const;

  float ic() const;

  float vce() const;

  void set_VCC(int _VCC);
};



