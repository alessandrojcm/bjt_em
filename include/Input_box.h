//
// Created by aless on 08/04/2018.
//

#pragma once
#include <nana/gui/widgets/textbox.hpp>
#include <nana/gui/msgbox.hpp>
#include <nana/gui/widgets/form.hpp>

template< class T >
class Input_box : public nana::textbox {
private:
  T _element;
public:
  explicit Input_box(nana::form &fm);

  T *get_element() const;
};

template< class T >
Input_box<T>::Input_box(nana::form &fm): nana::textbox(fm) {
  multi_lines(false);
  show();
}

template< class T >
T *Input_box<T>::get_element() const {
  std::string _value;
  getline(0, _value);

  try {
    return new T(std::stof(_value));
  } catch (...) {
    throw std::invalid_argument("Valor inválido.");
  }
}




