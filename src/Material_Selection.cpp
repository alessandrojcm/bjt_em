//
// Created by aless on 09/04/2018.
//

#include "Material_Selection.h"
Material_Selection::Material_Selection(const nana::form &fm) : combox(fm, nana::rectangle{100, 100, 100, 100}) {
  push_back(std::string("Silicio"));
  push_back(std::string("Germanio"));
  option(0);
}
MATERIAL Material_Selection::get_material() {
  if (option()==0) {
    return SILICIO;
  }

  return GERMANIO;
}
