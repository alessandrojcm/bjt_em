//
// Created by aless on 08/04/2018.
//

#include "BJT_PNP_EC.h"
BJT_PNP_EC::BJT_PNP_EC(const int &_beta, const MATERIAL &mat) : _beta(_beta) {
  set_material(mat);

  if (_beta <= 0) {
    throw std::invalid_argument("Valor de beta inválido");
  }
}
float BJT_PNP_EC::vbe() const {
  return _vbe;
}
int BJT_PNP_EC::beta() const {
  return _beta;
}
void BJT_PNP_EC::set_vbe(const MATERIAL &mat) {
  set_material(mat);
}
void BJT_PNP_EC::set_material(const MATERIAL &mat) {
  if (mat==SILICIO) {
    _vbe = 0.7;
  } else if (mat==GERMANIO) {
    _vbe = 0.3;
  }
}