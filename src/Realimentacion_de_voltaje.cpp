//
// Created by aless on 08/04/2018.
//

#include "Realimentacion_de_voltaje.h"
Realimentacion_de_voltaje::Realimentacion_de_voltaje(const resistencia &RC,
                                                     const resistencia &RB,
                                                     const resistencia &RE,
                                                     const BJT_PNP_EC &transistor)
    : _RC(RC), _RB(RB), _RE(RE), _transistor(transistor), _VCC(_VCC) {
}
float Realimentacion_de_voltaje::ib() const {
  return (_VCC - _transistor.vbe())
      /(_RB.resistividad() + _transistor.beta()*(_RC.resistividad() + _RE.resistividad()));
}
float Realimentacion_de_voltaje::ic() const {
  return (_transistor.beta() + 1)*ib();
}
float Realimentacion_de_voltaje::vce() const {
  return _VCC - ic()*(_RC.resistividad() + _RE.resistividad());
}
void Realimentacion_de_voltaje::set_VCC(int _VCC) {
  if (_VCC < 0) {
    throw std::invalid_argument("VCC no puede ser menor o igual que 0");
  }

  Realimentacion_de_voltaje::_VCC = _VCC;
}
