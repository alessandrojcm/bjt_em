//
// Created by aless on 09/04/2018.
//

#include <variant>
#include "Main_Window.h"

Main_Window::Main_Window(const nana::rectangle &fm, const nana::appearance &appear) : form(
    fm, appear), rc_input(*this), re_input(*this), rb_input(*this), transistor_input(*this), vcc_input(*this),
                                                                                      calculate(*this, true),
                                                                                      material_selection(*this),
                                                                                      transistor_label(*this),
                                                                                      circuito(*rc.get(),
                                                                                               *rb.get(),
                                                                                               *re.get(),
                                                                                               *transistor.get()) {
  caption("Realimentación de voltaje en colector-emisor");
  using namespace nana;
  //Etiquetas de texto
  transistor_label.caption("Material del transistor: ");

  // Entradas
  rc_input.tip_string("Resistencia RC: ");
  re_input.tip_string("Resistencia RE: ");
  rb_input.tip_string("Resistencia RB: ");
  transistor_input.tip_string("Beta del transistor: ");
  vcc_input.tip_string("VCC: ");

  //Botón
  calculate.caption("Calcular");

  //Evento para calcular
  calculate.events().click([&]() { calculate_circuit(); });

  //Posicionando los elementos
  place plc{*this};

  plc.div(
      "<><weight=80% vertical<><weight=70% vertical <vertical gap=10 textboxes arrange=[20,20,20,20,20]> <vertical gap=10 selection arrange=[25,25] margin=[20,0,0,10]> <weight=25 gap=10 buttons> ><>><>");

  plc.field("textboxes") << rc_input.handle() << rb_input.handle()
                         << re_input.handle()
                         << transistor_input.handle()
                         << vcc_input.handle();

  plc.field("selection") << transistor_label << material_selection.handle();

  plc.field("buttons") << calculate.handle();

  plc.collocate();
}
void Main_Window::calculate_circuit() {
  using namespace std;
  using namespace nana;

  vector<string> messages{};
  ostringstream os{};

  try {
    // Construyendo objetos a partir de entradas.
    rc.reset(rc_input.get_element());
    re.reset(re_input.get_element());
    rb.reset(rb_input.get_element());
    transistor.reset(transistor_input.get_element());
    transistor->set_vbe(material_selection.get_material());

    circuito = Realimentacion_de_voltaje(*rc.get(),
                                         *rb.get(),
                                         *re.get(),
                                         *transistor.get());
    circuito.set_VCC(*vcc_input.get_element());

    os << "Intensidad B: " << setprecision(2) << circuito.ib() << " A."<< endl;
    os << "Intensidad C: " << setprecision(2) << circuito.ic() << " A."<< endl;
    os << "Voltaje del colector-emisor: " << setprecision(2) << circuito.vce() << " V.";

    msgbox msg{"Resultados"};
    msg << os.str();
    msg.show();

  } catch (const exception &e) {
    msgbox msg{"¡Error!"};
    msg << e.what();
    msg.show();
  }
}