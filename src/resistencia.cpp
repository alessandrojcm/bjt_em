//
// Created by aless on 07/04/2018.
//

#include "resistencia.h"
resistencia::resistencia(float _resistividad)
    : _resistividad(_resistividad) {
  if (_resistividad < 0) {
    throw std::invalid_argument("Los valores de la resistencia no pueden ser menores a 0.");
  }
}

resistencia::resistencia(const resistencia &resistencia) {
  this->_resistividad = resistencia._resistividad;
}
