#include <nana/gui.hpp>
#include <iostream>
#include "Main_Window.h"

int main(int argc, char **argv) {
  using namespace nana;

  try {
    Main_Window mw{rectangle{100, 100, 400, 400}, appearance(true, false, true, false, true, false, false)};;

    mw.show();

    exec();
  } catch (const std::exception& e){
    std::cerr << e.what();
  }

  return 0;
}